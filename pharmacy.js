export class Drug {
  constructor(name, expiresIn, benefit) {
    this.name = name;
    this.expiresIn = expiresIn;
    this.benefit = benefit;
  }
}

export class Pharmacy {
  constructor(drugs = []) {
    this.drugs = drugs;
  }
  modifyBenefitValue(index, value) {
    this.drugs[index].benefit = this.drugs[index].benefit + value;
    if (this.drugs[index].benefit > 50) {
      this.drugs[index].benefit = 50;
    } else if (this.drugs[index].benefit < 0) {
      this.drugs[index].benefit = 0;
    }
  }
  fervexHandler(index) {
    if (this.drugs[index].expiresIn < 6) {
      this.modifyBenefitValue(index, 3);
    } else if (this.drugs[index].expiresIn < 11) {
      this.modifyBenefitValue(index, 2);
    } else {
      this.modifyBenefitValue(index, 1);
    }
  }
  modifyExpiration(index) {
    if (this.drugs[index].name != "Magic Pill") {
      this.drugs[index].expiresIn = this.drugs[index].expiresIn - 1;
      if (this.drugs[index].expiresIn < 0) {
        switch (this.drugs[index].name) {
          case "Fervex":
            this.drugs[index].benefit = 0;
            break;
          case "Herbal Tea":
            this.modifyBenefitValue(index, 1);
            break;
          case "Dafalgan":
            this.modifyBenefitValue(index, -2);
            break;
          default:
            this.modifyBenefitValue(index, -1);
            break;
        }
      }
    }
  }
  updateBenefitValue() {
    for (var i = 0; i < this.drugs.length; i++) {
      switch (this.drugs[i].name) {
        case "Fervex":
          this.fervexHandler(i);
          break;
        case "Herbal Tea":
          this.modifyBenefitValue(i, 1);
        case "Magic Pill":
          break;
        case "Dafalgan":
          this.modifyBenefitValue(i, -2);
          break;
        default:
          this.modifyBenefitValue(i, -1);
      }
      this.modifyExpiration(i);
    }
    return this.drugs;
  }
}

  /*  for (var i = 0; i < this.drugs.length; i++) {
      if (
        this.drugs[i].name != "Herbal Tea" &&
        this.drugs[i].name != "Fervex"
      ) {
        if (this.drugs[i].benefit > 0) {
          if (this.drugs[i].name != "Magic Pill" &&
              this.drugs[i].name != "Dafalgan") {
            this.drugs[i].benefit = this.drugs[i].benefit - 1;
          } else if (this.drugs[i].name == "Dafalgan") {
            this.drugs[i].benefit = this.drugs[i].benefit - 2;
          }
        }
      } else {
        if (this.drugs[i].benefit < 50) {
          this.drugs[i].benefit = this.drugs[i].benefit + 1;
          if (this.drugs[i].name == "Fervex") {
            if (this.drugs[i].expiresIn < 11) {
              if (this.drugs[i].benefit < 50) {
                this.drugs[i].benefit = this.drugs[i].benefit + 1;
              }
            }
            if (this.drugs[i].expiresIn < 6) {
              if (this.drugs[i].benefit < 50) {
                this.drugs[i].benefit = this.drugs[i].benefit + 1;
              }
            }
          }
        }
      }
      if (this.drugs[i].name != "Magic Pill") {
        this.drugs[i].expiresIn = this.drugs[i].expiresIn - 1;
      }
      if (this.drugs[i].expiresIn < 0) {
        if (this.drugs[i].name != "Herbal Tea") {
          if (this.drugs[i].name != "Fervex") {
            if (this.drugs[i].benefit > 0) {
              if (this.drugs[i].name != "Magic Pill" &&
                  this.drugs[i].name != "Dafalgan") {
                this.drugs[i].benefit = this.drugs[i].benefit - 1;
              } else if (this.drugs[i].name == "Dafalgan") {
                this.drugs[i].benefit = this.drugs[i].benefit - 2;
              }
            }
          } else {
            this.drugs[i].benefit =
              this.drugs[i].benefit - this.drugs[i].benefit;
          }
        } else {
          if (this.drugs[i].benefit < 50) {
            this.drugs[i].benefit = this.drugs[i].benefit + 1;
          }
        }
      }
    }

    return this.drugs;
  }*/
